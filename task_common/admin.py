# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from task_common.models import Task


class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'completed')


admin.site.register(Task, TaskAdmin)
