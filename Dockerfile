# Dockfile
FROM frolvlad/alpine-python3

# Move files to proper places
COPY . /app/

# Application requirements install
RUN apk add --no-cache bash && \
    apk add --no-cache netcat-openbsd && \
    pip3 install gunicorn && \
    apk add --no-cache mariadb-dev g++ && \
    apk add --no-cache python3-dev && \
    apk add --no-cache zlib-dev && \
    apk add --no-cache libffi-dev build-base && \
    pip3 install -r /app/requirements.txt && \
    apk del g++ mariadb-dev && \
    apk add --no-cache mariadb-client-libs

# Assign execution permissions
RUN chmod 744 /app/docker-entrypoint.sh

# Expose web port
EXPOSE 80

CMD ["/app/docker-entrypoint.sh"]