# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.test import TestCase

from task_backend.serializers import TaskSerializer
from task_common.models import Task


class TaskSerializerTest(TestCase):
    """ The tests for Task model serializer. """

    def test_task_create(self):
        """ Uses serializer to create task given name. Checks whether completed has been properly
            populated.
        """
        task_name = "First Task"

        serializer = TaskSerializer(data={'name': task_name})

        self.assertEqual(serializer.is_valid(), True)

        task = serializer.create(serializer.validated_data)

        get_task = Task.objects.get(id=task.id)

        self.assertEqual(get_task.name, task_name)
        self.assertEqual(get_task.completed, False)


class TaskRESTAPITest(TestCase):
    """ The Task REST API test cases. """
    task_name = 'First Task'

    def test_list_empty(self):
        """ The list operation when there is no tasks. """
        response = self.client.get('/api/task/')

        self.assertEqual(response.status_code, 200)

        resp_data = json.loads(response.content)

        self.assertEqual(len(resp_data), 0)

    def test_list(self):
        """ The list operation when there is one task. """
        task = self.create_task()

        response = self.client.get('/api/task/')

        self.assertEqual(response.status_code, 200)

        resp_data = json.loads(response.content)

        self.assertEqual(len(resp_data), 1)
        self.assertEqual(task.id, resp_data[0]['id'])
        self.assertEqual(task.name, resp_data[0]['name'])
        self.assertEqual(task.completed, resp_data[0]['completed'])

    def test_get(self):
        """ The get operation on existing task. """
        task = self.create_task()

        response = self.client.get('/api/task/{}/'.format(task.id))

        self.assertEqual(response.status_code, 200)

        resp_data = json.loads(response.content)

        self.assertEqual(task.id, resp_data['id'])
        self.assertEqual(task.name, resp_data['name'])
        self.assertEqual(task.completed, resp_data['completed'])

    def test_get_non_existing_id(self):
        """ The get operation on non-existing task. """
        response = self.client.get('/api/task/{}/'.format(1))

        self.assertEqual(response.status_code, 404)

    def test_put(self):
        """ The put operation test. """
        put_name = 'Put Task'
        put_completed = True
        task = self.create_task()

        response = self.client.put(
            '/api/task/{}/'.format(task.id),
            json.dumps({
                'name': put_name,
                'completed': put_completed
            }),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)

        resp_data = json.loads(response.content)
        self.assertEqual(put_name, resp_data['name'])
        self.assertEqual(put_completed, resp_data['completed'])

    def test_put_name_missing(self):
        """ The put operation test. """
        put_completed = True
        task = self.create_task()

        response = self.client.put(
            '/api/task/{}/'.format(task.id),
            json.dumps({'completed': put_completed}),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 400)

    def test_put_non_existing_id(self):
        """ The put operation test with non existent id. """
        put_name = 'Put Task'
        put_completed = True

        response = self.client.put(
            '/api/task/{}/'.format(1),
            json.dumps({
                'name': put_name,
                'completed': put_completed
            }),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 404)

    def test_patch(self):
        """ The patch operation test. """
        patch_name = 'Patch Task'
        task = self.create_task()

        response = self.client.patch(
            '/api/task/{}/'.format(task.id),
            json.dumps({
                'name': patch_name,
            }),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)

        resp_data = json.loads(response.content)
        self.assertEqual(patch_name, resp_data['name'])
        self.assertEqual(task.completed, resp_data['completed'])

    def test_patch_name_missing(self):
        """ The patch operation test. """
        patch_completed = True
        task = self.create_task()

        response = self.client.patch(
            '/api/task/{}/'.format(task.id),
            json.dumps({
                'completed': patch_completed,
            }),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)

        resp_data = json.loads(response.content)
        self.assertEqual(task.name, resp_data['name'])
        self.assertEqual(patch_completed, resp_data['completed'])

    def test_patch_non_existing_id(self):
        """ The patch operation test with non existent id. """
        patch_completed = True

        response = self.client.patch(
            '/api/task/{}/'.format(1),
            json.dumps({
                'completed': patch_completed,
            }),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 404)

    def test_delete(self):
        """ The delete operation test."""
        task = self.create_task()

        response = self.client.delete('/api/task/{}/'.format(task.id))

        self.assertEqual(response.status_code, 204)

        in_database = True
        try:
            Task.objects.get(id=task.id)
        except Task.DoesNotExist:
            in_database = False

        self.assertEqual(in_database, False)

    def test_delete_non_existing_id(self):
        """ The delete operation test with non existent id."""
        response = self.client.delete('/api/task/{}/'.format(1))

        self.assertEqual(response.status_code, 404)

    def create_task(self):
        task = Task(name=self.task_name)
        task.save()

        return task
