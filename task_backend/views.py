# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets

from task_backend.serializers import TaskSerializer

from task_common import models as common_models


class TaskViewSet(viewsets.ModelViewSet):
    queryset = common_models.Task.objects.all()
    serializer_class = TaskSerializer
