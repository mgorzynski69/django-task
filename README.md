# General

This repository contains recruitment task given by company *********.

## Prerequisites

1. docker
2. python2
3. fabric

## Building Docker Image

The docker image can be build using command given below:
```bash
fab build
```

## Running Test Server in Docker

The test server is being run using docker-compose and requires port 80 to run successfully. The first
startup of the server may take a few minutes. The MySQL database requires setup & web server is
waiting for it.

Start server:
```bash
fab start_server
```

Stop server:
```bash
fab stop_server
```

## Troubleshooting

1. How can I change default test server port?

Please go to docker-compose.yml and edit ports section in task service.