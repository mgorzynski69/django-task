#!/bin/bash

echo "Waiting for database..."
while ! nc -w 1 -z $DB_HOST $DB_PORT; do sleep 1; done;
echo "Database is up and running..."

# cd to app directory
cd /app/

# collect static files
echo "Collecting static files..."
python3 manage.py collectstatic --no-input

# migrate db changes
echo "Migrating changes to database..."
python3 manage.py migrate

# run web server
echo "Run web server..."
gunicorn -b 0.0.0.0:80 task.wsgi