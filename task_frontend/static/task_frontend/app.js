var app = angular.module("TaskApp", []);

app.controller("TaskController", ["$scope", "$http", function($scope, $http) {
    $scope.tasks = [];

    $http.get("/api/task/", {"Content-Type": "application/json"}).then(
        function success(response) {
            console.log(response.data);
            $scope.tasks = response.data;
        },
        function error(response) {
            console.log("Error - GET /api/task/", response);
        }
    );

    $scope.createTask = function () {
        $http.post(
            "/api/task/",
            {"name": $scope.taskName },
            {"Content-Type": "application/json"}
        ).then(
            function success(response) {
                console.log(response.data);
                $scope.tasks.push(response.data);
                $scope.taskName = "";
            },
            function error(response) {
                console.log("Error - POST /api/task/", response);
            }
        )
    };

    $scope.completeTask = function (task, completed) {
        $http.patch(
            "/api/task/" + task.id + "/",
            {"completed": completed },
            {"Content-Type": "application/json"}
        ).then(
            function success(response) {
                console.log(response.data);
                task.completed = completed;
            },
            function error(response) {
                console.log("Error - PATCH /api/task/", response);
            }
        )
    };

    $scope.editTask = function (task) {
        task.edit = true;
    };

    $scope.finishEditTask = function (task) {
        $http.patch(
            "/api/task/" + task.id + "/",
            {"name": task.name},
            {"Content-Type": "application/json"}
        ).then(
            function success(response) {
                console.log(response.data);
                task.edit = false;
            },
            function error(response) {
                console.log("Error - PATCH /api/task/", response);
            }
        )
    };

    $scope.deleteTask = function (task) {
        $http.delete(
            "/api/task/" + task.id + "/",
            {"Content-Type": "application/json"}
        ).then(
            function success(response) {
                console.log(response.data);
                $scope.tasks.splice($scope.tasks.indexOf(task), 1);
            },
            function error(response) {
                console.log("Error - DELETE /api/task/", response);
            }
        )
    };

}]);