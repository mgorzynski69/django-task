# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from task_common.models import Task


class TaskModelTests(TestCase):
    """ The Task Model test cases. """

    def test_task_create_only_name(self):
        """ Creates task given name. Checks if completes is assigned proper default. """
        task_name = 'First Task'

        new_task = Task(name=task_name)
        new_task.save()

        get_task = Task.objects.get(id=new_task.id)

        self.assertEqual(get_task.name, task_name)
        self.assertEqual(get_task.completed, False)
