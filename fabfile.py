from fabric.api import local

# Docker Image Settings
ORGANIZATION = 'task'
IMAGE = 'task'


def build(tag='latest'):
    local("docker build . --tag {}/{}:{} ".format(ORGANIZATION, IMAGE, tag))


def start_server():
    local("docker-compose up -d")


def stop_server():
    local("docker-compose down")
